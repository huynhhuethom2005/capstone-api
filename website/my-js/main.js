import { renderProduct, renderIphone, renderSasung, countCart } from "../my-js/controller.js"

const BASE_URL = "https://63d72c8a5c4274b136efaefe.mockapi.io/product_captone"

let fetchProduct = () => {
    document.getElementById("loading-text").style.display = "block"
    document.getElementById("shopAll").innerHTML = ""
    axios({
        url: BASE_URL,
        method: 'GET',
    }).then((result) => {
        renderProduct(result.data)
        document.getElementById("loading-text").style.display = "none"
    }).catch((err) => {
        console.log('err :>> ', err);
    });
}
fetchProduct()

let filterIphone = () => {
    document.getElementById("loading-text").style.display = "block"
    document.getElementById("shopAll").innerHTML = ""
    axios({
        url: BASE_URL,
        method: 'GET',
    }).then((result) => {
        renderIphone(result.data)
        document.getElementById("loading-text").style.display = "none"
    }).catch((err) => {
        console.log('err :>> ', err);
    });
}

let filterSasung = () => {
    document.getElementById("loading-text").style.display = "block"
    document.getElementById("shopAll").innerHTML = ""
    axios({
        url: BASE_URL,
        method: 'GET',
    }).then((result) => {
        renderSasung(result.data)
        document.getElementById("loading-text").style.display = "none"
    }).catch((err) => {
        console.log('err :>> ', err);
    });
}

var Cart = []
let addCart = (id) => {
    axios({
        url: `${BASE_URL}/${id}`,
        method: 'GET',
    }).then((result) => {
        let data = result.data
        let cartitem = {
            id: data.id,
            name: data.name,
            price: data.price,
            screen: data.screen,
            backCamera: data.backCamera,
            frontCamera: data.frontCamera,
            img: data.img,
            desc: data.desc,
            type: data.typefalse,
            quantity: 1,
        }
        if (Cart.length == 0) {
            Cart.push(cartitem);
        } else {
            let eleCart = Cart.find(ele => ele.id == id)
            if (eleCart) {
                eleCart.quantity++
            } else {
                Cart.push(cartitem);
            }
        }
        countCart(Cart)
        Toastify({
            text: "Add success!",
            className: "info",
            duration: 1000,
            style: {
                background: "linear-gradient(to right, #00b09b, #96c93d)",
            }
        }).showToast();
        localStorage.setItem("cartStore", JSON.stringify(Cart));
    }).catch((err) => {
        console.log(err)
    });
}

let cartJson = localStorage.getItem("cartStore");
if (cartJson != null) {
    Cart = JSON.parse(cartJson)
}
countCart(Cart)
// countCart(localStorage.getItem("cartStore"))

window.filterIphone = filterIphone
window.filterSasung = filterSasung
window.fetchProduct = fetchProduct
window.addCart = addCart

