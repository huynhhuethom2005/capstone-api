export let renderProduct = (data) => {
    let contentHTML = ""
    data.forEach((item) => {
        let contentBody = `
            <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                <div class="block-4 text-center border">
                <figure class="block-4-image">
                    <img src="${item.img}" alt="Image placeholder" class="img-fluid">
                </figure>
                <div class="block-4-text">
                    <h3><a href="#">${item.name}</a></h3>
                    <p class="mb-0">${item.screen}</p>
                    <p class="mb-0">${item.backCamera}</p>
                    <p class="mb-0">${item.frontCamera}</p>
                    ${item.type ? `<p class="mb-0 text-danger">Iphone</p>` : `<p class="mb-0 text-warning">Samsung</p>`} 
                    <p class="text-primary font-weight-bold">$${item.price}</p>
                </div>
                <button class="mb-3 btn btn-success" onclick="addCart(${item.id})">ADD TO CART</button>
                </div>
            </div>    
            `
        contentHTML += contentBody
    })
    document.getElementById("shopAll").innerHTML = contentHTML
}

export let renderIphone = (data) => {
    let contentIphone = ""
    data.forEach((item) => {
        if (item.type == true) {
            let contentBody = `
            <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                <div class="block-4 text-center border">
                <figure class="block-4-image">
                    <img src="${item.img}" alt="Image placeholder" class="img-fluid">
                </figure>
                <div class="block-4-text">
                    <h3><a href="#">${item.name}</a></h3>
                    <p class="mb-0">${item.screen}</p>
                    <p class="mb-0">${item.backCamera}</p>
                    <p class="mb-0">${item.frontCamera}</p>
                    ${item.type ? `<p class="mb-0 text-danger">Iphone</p>` : `<p class="mb-0 text-warning">Samsung</p>`} 
                    <p class="text-primary font-weight-bold">$${item.price}</p>
                </div>
                <button class="mb-3 btn btn-success" onclick="addCart(${item.id})">ADD TO CART</button>
                </div>
            </div>    
            `
            contentIphone += contentBody
        }
    })
    document.getElementById("shopAll").innerHTML = contentIphone
}

export let renderSasung = (data) => {
    let contentSamsung = ""
    data.forEach((item) => {
        if (item.type == false) {
            let contentBody = `
            <div class="col-sm-6 col-lg-4 mb-4" data-aos="fade-up">
                <div class="block-4 text-center border">
                <figure class="block-4-image">
                    <img src="${item.img}" alt="Image placeholder" class="img-fluid">
                </figure>
                <div class="block-4-text">
                    <h3><a href="#">${item.name}</a></h3>
                    <p class="mb-0">${item.screen}</p>
                    <p class="mb-0">${item.backCamera}</p>
                    <p class="mb-0">${item.frontCamera}</p>
                    ${item.type ? `<p class="mb-0 text-danger">Iphone</p>` : `<p class="mb-0 text-warning">Samsung</p>`} 
                    <p class="text-primary font-weight-bold">$${item.price}</p>
                </div>
                <button class="mb-3 btn btn-success" onclick="addCart(${item.id})">ADD TO CART</button>
                </div>
            </div>    
            `
            contentSamsung += contentBody
        }
    })
    document.getElementById("shopAll").innerHTML = contentSamsung
}

export let countCart = (array) => {
    let countItem = 0
    array.forEach(val => {
        countItem += val.quantity
    })
    document.getElementById("count").innerHTML = countItem
}

// Cart details
let cartJson = localStorage.getItem("cartStore");
export let renderCart = (data) => {
    let contentCart = ""
    data.forEach(val => {
        let contentTr = `
        <tr>
            <td class="product-thumbnail">
            <img src="${val.img}" alt="Image" class="img-fluid">
            </td>
            <td class="product-name">
            <h2 class="h5 text-black">${val.name}</h2>
            </td>
            <td>$${val.price}</td>
            <td>
            <div class="input-group mb-3" style="max-width: 120px;">
                <div class="input-group-prepend">
                <button onclick="decreaseProduct(${val.id})" class="btn btn-outline-primary js-btn-minus" type="button">&minus;</button>
                </div>
                <input id="${val.id}" type="text" class="form-control text-center" value="${val.quantity}" placeholder=""
                aria-label="Example text with button addon" aria-describedby="button-addon1">
                <div class="input-group-append">
                <button onclick="increaseProduct(${val.id})" class="btn btn-outline-primary js-btn-plus" type="button">&plus;</button>
                </div>
            </div>

            </td>
            <td>${val.backCamera}</td>
            <td>${val.desc}</td>
            <td>${val.frontCamera}</td>
            <td>${val.screen}</td>
            <td><button type="button" onclick="removeCart(${val.id})" class="btn btn-primary">X</button></td>
        </tr>
        `
        contentCart += contentTr
    })
    document.getElementById("cartAll").innerHTML = contentCart
}
