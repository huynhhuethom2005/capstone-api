import { countCart, renderCart } from "../my-js/controller.js"

let cartJson = JSON.parse(localStorage.getItem("cartStore"));

countCart(cartJson)
renderCart(cartJson)

let increaseProduct = (id) => {
    let value = document.getElementById(id).value * 1 + 1
    let eleCart = cartJson.find(ele => ele.id == id)
    eleCart.quantity = value
    localStorage.setItem("cartStore", JSON.stringify(cartJson));
    countCart(cartJson)
}

let decreaseProduct = (id) => {
    let value = document.getElementById(id).value * 1 - 1
    let eleCart = cartJson.find(ele => ele.id == id)
    eleCart.quantity = value
    localStorage.setItem("cartStore", JSON.stringify(cartJson));
    countCart(cartJson)
}

let removeCart = (id) => {
    const obj = cartJson.findIndex((obj) => obj.id == id);

    if (obj > -1) {
        cartJson.splice(obj, 1);
    }
    console.log('cartJson :>> ', cartJson);
    localStorage.setItem("cartStore", JSON.stringify(cartJson));
    countCart(cartJson)
    renderCart(cartJson)
    Toastify({
        text: "Delete success!",
        className: "info",
        duration: 1000,
        style: {
            background: "linear-gradient(to right, #00b09b, #96c93d)",
        }
    }).showToast();
}

window.increaseProduct = increaseProduct
window.decreaseProduct = decreaseProduct
window.removeCart = removeCart

