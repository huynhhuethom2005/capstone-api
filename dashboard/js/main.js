import { renderListProducts, getInfo } from '../js/controller.js'
import { checkEmpty, checkNumber } from '../js/validation.js'

let BASE_URL = "https://63d67b1094e769375bb1f8c7.mockapi.io/"

renderListProducts(BASE_URL)

let resetForm = () => {
    document.getElementById('giaMon').value = ''
    document.getElementById('tenMon').value = ''
    document.getElementById('hinhAnh').value = ''
    document.getElementById('moTa').value = ''

}

window.resetForm = resetForm;

let resetWarningText = () => {
    document.getElementById('warninggiaMon').innerHTML = ''
    document.getElementById('warningTenMon').innerHTML = ''
    document.getElementById('warninghinhAnh').innerHTML = ''
    document.getElementById('warningMoTa').innerHTML = ''
}

let addProduct = () => {
    let valueCheck = checkNumber('giaMon', 'warninggiaMon') && checkEmpty('giaMon', 'warninggiaMon') &
        checkEmpty('hinhAnh', 'warninghinhAnh') &
        checkEmpty('moTa', 'warningMoTa') &
        checkEmpty('tenMon', 'warningTenMon')
    if (valueCheck == 1) {
        axios({
            url: `${BASE_URL}/food`,
            method: "POST",
            data: getInfo()
        })
            .then((res) => {
                console.log(res)
                renderListProducts(BASE_URL)
                Toastify({
                    text: "Thêm sản phẩm thành công",
                    className: "info",
                    style: {
                        background: "linear-gradient(to right, #00b09b, #96c93d)",
                    },
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                }).showToast();
                $('#exampleModalCenter').modal('hide')
            })
            .catch(err => {
                console.log(err)
            })
    }

}

$('#exampleModalCenter').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
})

window.addProduct = addProduct;

let removeProduct = (id) => {
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE"
    })
        .then(res => {
            console.log(res)
            renderListProducts(BASE_URL)
            Toastify({
                text: "Xóa sản phẩm thành công",
                className: "info",
                style: {
                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                },
                offset: {
                    x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                    y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                },
            }).showToast();
        })
        .catch(err => {
            console.log(err)
        })
}

window.removeProduct = removeProduct

let updateProduct = () => {
    let dataToUpdate = getInfo()
    let valueCheck = checkNumber('giaMon', 'warninggiaMon') && checkEmpty('giaMon', 'warninggiaMon') &
        checkEmpty('hinhAnh', 'warninghinhAnh') &
        checkEmpty('moTa', 'warningMoTa') &
        checkEmpty('tenMon', 'warningTenMon')
    if (valueCheck == 1) {
        axios({
            url: `${BASE_URL}/food/${dataToUpdate.maMon}`,
            method: 'PUT',
            data: dataToUpdate
        })
            .then(res => {
                console.log(res)
                renderListProducts(BASE_URL)
                Toastify({
                    text: "Cập nhật sản phẩm thành công",
                    className: "info",
                    style: {
                        background: "linear-gradient(to right, #00b09b, #96c93d)",
                    },
                    offset: {
                        x: 50, // horizontal axis - can be a number or a string indicating unity. eg: '2em'
                        y: 10 // vertical axis - can be a number or a string indicating unity. eg: '2em'
                    },
                }).showToast();
                $('#exampleModalCenter').modal('hide');
            })
            .then(res => {
                console.log(res)
            })
    }

}

window.updateProduct = updateProduct

let editProduct = (id) => {
    resetWarningText()
    $('#exampleModalCenter').modal('show')
    document.getElementById('addProduct').classList.add('d-none')
    document.getElementById('updateProduct').classList.remove('d-none')
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "GET",
    })
        .then(response => {
            console.log(response)
            let data = response.data
            document.getElementById('tenMon').value = data.tenMon
            document.getElementById('giaMon').value = data.giaMon
            document.getElementById('hinhAnh').value = data.hinhAnh
            document.getElementById('moTa').value = data.moTa
            document.getElementById('maMon').value = data.maMon
        })
        .catch(err => {
            console.log(err)
        })
}

window.editProduct = editProduct

