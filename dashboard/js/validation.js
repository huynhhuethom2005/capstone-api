export let checkEmpty = (idItem, idDivShowTextWarning) => {
    let valueItem = document.getElementById(idItem);
    let itemToWarning = document.getElementById(idDivShowTextWarning);

    if (valueItem.value == "") {
        itemToWarning.innerHTML = " * Không được để trống * "
        return false;
    } else {
        itemToWarning.innerHTML = ""
        return true;
    }
}

export let checkNumber = (idItem, idDivShowTextWarning) => {
    let valueItem = document.getElementById(idItem).value;
    let itemToWarning = document.getElementById(idDivShowTextWarning);

    if (isNaN(valueItem)) {
        itemToWarning.innerHTML = " Phải là số "
        return false;
    } else {
        itemToWarning.innerHTML = ""
        return true;
    }
}
