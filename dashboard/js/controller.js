
let renderContentTr = (listProduct) => {
    let contentHTML = ``;
    listProduct.forEach(item => {
        contentHTML += `
        <tr>
            <td>${item.maMon}</td>
            <td>${item.tenMon}</td>
            <td>${item.giaMon}</td>
            <td>${item.hinhAnh}</td>
            <td>${item.moTa}</td>
            <td>
                <button onclick="removeProduct(${item.maMon})" class="btn btn-danger">Xóa</button>
                <button onclick="editProduct(${item.maMon})"class="btn btn-success">Sửa</button>
            </td>
        </tr>`
    })
    document.getElementById('contentBody').innerHTML = contentHTML
}

export let renderListProducts = (url) => {
    axios({
        url: `${url}/food`,
        method: 'GET',
    })
        .then((res) => {
            var data = res.data
            renderContentTr(data)
            console.log(data)
        })
        .catch((err) => {
            console.log(err)
        })
}

export let getInfo = () => {
    let tenMon = document.getElementById('tenMon').value
    let giaMon = document.getElementById('giaMon').value
    let hinhAnh = document.getElementById('hinhAnh').value
    let moTa = document.getElementById('moTa').value
    let maMon = document.getElementById('maMon').value
    return {
        tenMon, giaMon, hinhAnh, moTa, maMon
    }
}


